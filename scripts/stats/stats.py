###########################################################
#                                                         #
#               Statistics Implementations                #
#                                                         #
###########################################################

import numpy as np

# 1. R-squared
#
#
#


###########################################################
#                                                         #
###########################################################

def model(bvals, params):
    """
    """

    d = params[0]
    pd = params[1]
    f = params[2]

    return (1-f)*np.exp(-bvals*d)+f*np.exp(-bvals*pd)


def residuals(bvals, params, data):
    """
    """

    return data - model(bvals, params)


def rsquared(bvals, data, params):
    """

    Params:
        - data
        - params
    """
    
    if(len(np.array(data).shape) == 1):

        y_mean = np.mean(data)
        ssres = sum(residuals(bvals, params, data)**2)
        sstot = sum((data - y_mean)**2)

        rsqd = 1 - ssres/sstot

        return rsqd        
    
    else:
        
        d, d_star, f = params
        
        rsqd = []

        for i in range(data.shape[0]):    
            for j in range(data.shape[1]):    
                for k in range(data.shape[2]):
                    y_mean = np.mean(data[i,j,k])
                    ssres = sum(residuals(bvals, [d[i,j,k],d_star[i,j,k],f[i,j,k]], data[i,j,k])**2)
                    sstot = sum((data[i,j,k] - y_mean)**2)
                    
                    rsqd.append(1 - ssres/sstot)

        rsqd_mean = np.mean(rsqd)
        
        return rsqd_mean
